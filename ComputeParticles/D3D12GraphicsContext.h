#pragma once

#include <d3d12.h>
#include <dxgi1_4.h>
#include <d3dcompiler.h>
#include "ParticlesSystem.h"
#include "D3D12UtilityFunctions.h"
#include "Matrix.h"

namespace graphics {

	struct Color {
		float color[4];
	};

	struct ViewProj {
		math::Matrix4x4 vp;
	};

	struct Vertex {
		float position[4];
		float texcoord[2];
	};

	struct ContextDesc {
		HWND hWnd;
		unsigned int width, height, rr;
	};

	class D3D12GraphicsContext
	{
	public:
		D3D12GraphicsContext(ContextDesc contextDesc);
		~D3D12GraphicsContext() {};

		void DrawParticles();
	private:

		void BindComputeResources();
		void BindGraphicsResources();

		void CreateConstantBuffer();
		void CreateScreenQuad();

		void CreateGraphicsRootSignature();
		void CreateGraphicsPipeline();

		void CreateComputeRootSignature();
		void CreateComputePipeline();
	private:
		unsigned int tableOffset;
		unsigned int currentFrameIndex;
		IDXGISwapChain3* swapChain;
		IDXGIFactory1* factory;

		HANDLE fenceEvent;
		ID3D12Fence* fence[3];
		unsigned long long fenceValue[3];

		ID3D12Device* device;
		ID3D12GraphicsCommandList* cmdList;
		ID3D12CommandAllocator* cmdAlloc[3];
		ID3D12CommandQueue* cmdQueue;

		ID3D12Resource* rtvTextures[3];
		ID3D12DescriptorHeap* rtvDescriptorHeap;
		size_t rtvDescriptorOffsetSize;

		D3D12_VIEWPORT view;
		D3D12_RECT scissorRect;

		ID3D12DescriptorHeap* cbvSrvUavNonShaderVisibleHeap;
		ID3D12DescriptorHeap* cbvSrvUavShaderVisibleHeap;
		size_t cbvSrvUavDescriptorOffsetSize;
		size_t currentDescriptorsAllocated;

		ID3D12PipelineState* pso;
		ID3D12RootSignature* rootSig;

		ID3D12PipelineState* computePso;
		ID3D12RootSignature* computeRootSig;

		graphics::SrvUav* uav;
		graphics::SrvUav* srv1;
		graphics::SrvUav* srv2;
		graphics::ScreenQuad* screenQuad;
		
		ViewProj viewProj;
		graphics::StructuredConstantBuffer viewProjCB;
		void* pGpuMappedAddress;

		ParticleSystem* particleSystem;

		math::Matrix4x4 viewMat, projection;
		math::Matrix4x4 vp;
	};

};