#include "D3D12UtilityFunctions.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include <d3d12.h>
using namespace graphics;

SrvUav* graphics::CreateUav(ID3D12Device* device, ID3D12GraphicsCommandList* cmdList, ID3D12DescriptorHeap* heap, size_t *currentDescriptorsAllocated, unsigned int cbvSrvUavDescriptorOffsetSize, unsigned width, unsigned height, DXGI_FORMAT format) {
	
	SrvUav* srvUav = new SrvUav();
	srvUav->texture = new Texture();

	D3D12_RESOURCE_DESC resourceDesc{};
	resourceDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	resourceDesc.Alignment = 0;
	resourceDesc.DepthOrArraySize = 1;
	resourceDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	resourceDesc.Height = height;
	resourceDesc.Width = width;
	resourceDesc.MipLevels = 1;
	resourceDesc.SampleDesc.Count = 1;
	resourceDesc.SampleDesc.Quality = 0;
	resourceDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS;

	CD3DX12_HEAP_PROPERTIES heapPropertiesDefault = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);

	device->CreateCommittedResource(&heapPropertiesDefault,
		D3D12_HEAP_FLAG_NONE,
		&resourceDesc,
		D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE,
		nullptr,
		IID_PPV_ARGS(&srvUav->texture->texture));

	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.Texture2D.MipLevels = 1;
	srvDesc.Texture2D.MostDetailedMip = 0;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;

	D3D12_UNORDERED_ACCESS_VIEW_DESC uavDesc{};
	uavDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	uavDesc.ViewDimension = D3D12_UAV_DIMENSION_TEXTURE2D;
	uavDesc.Texture2D.MipSlice = 0;

	size_t dcurrentDescriptorsAllocated = *currentDescriptorsAllocated;

	D3D12_CPU_DESCRIPTOR_HANDLE heapHandle = heap->GetCPUDescriptorHandleForHeapStart();
	heapHandle.ptr = heapHandle.ptr + (dcurrentDescriptorsAllocated * cbvSrvUavDescriptorOffsetSize);

	device->CreateUnorderedAccessView(srvUav->texture->texture, nullptr, &uavDesc, heapHandle);

	srvUav->uavHeapIndex = *currentDescriptorsAllocated;
	(*currentDescriptorsAllocated)++;

	dcurrentDescriptorsAllocated++;

	heapHandle = heap->GetCPUDescriptorHandleForHeapStart();
	heapHandle.ptr = heapHandle.ptr + (dcurrentDescriptorsAllocated * cbvSrvUavDescriptorOffsetSize);

	device->CreateShaderResourceView(srvUav->texture->texture, &srvDesc, heapHandle);

	srvUav->cbvSrvUavHeapIndex = *currentDescriptorsAllocated;
	(*currentDescriptorsAllocated)++;

	return srvUav;
}

SrvUav* graphics::CreateSRVFromImage(ID3D12Device* device, ID3D12GraphicsCommandList* cmdList, D3D12_CPU_DESCRIPTOR_HANDLE cbvSrvUavHandle, unsigned int cbvSrvUavIndex, const char* filename) {

	int width, height, cc;
	void* pData = stbi_load(filename, &width, &height, &cc, 4);

	D3D12_RESOURCE_DESC resourceDesc{};
	resourceDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	resourceDesc.Alignment = 0;
	resourceDesc.DepthOrArraySize = 1;
	resourceDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	resourceDesc.Height = height;
	resourceDesc.Width = width;
	resourceDesc.MipLevels = 1;
	resourceDesc.SampleDesc.Count = 1;
	resourceDesc.SampleDesc.Quality = 0;

	unsigned long long textureUploadSize;
	device->GetCopyableFootprints(&resourceDesc, 0, 1, 0, nullptr, 0, 0, &textureUploadSize);

	CD3DX12_HEAP_PROPERTIES heapPropertiesUpload  = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);
	CD3DX12_HEAP_PROPERTIES heapPropertiesDefault = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);
	CD3DX12_RESOURCE_DESC textureUploadResource = CD3DX12_RESOURCE_DESC::Buffer(textureUploadSize);

	SrvUav* srv = new SrvUav();
	srv->texture = new Texture();
	device->CreateCommittedResource(&heapPropertiesDefault, 
		D3D12_HEAP_FLAG_NONE, 
		&resourceDesc, 
		D3D12_RESOURCE_STATE_COPY_DEST, 
		nullptr, 
		IID_PPV_ARGS(&srv->texture->texture));

	device->CreateCommittedResource(&heapPropertiesUpload,
		D3D12_HEAP_FLAG_NONE,
		&textureUploadResource,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&srv->texture->textureUpload));

	D3D12_SUBRESOURCE_DATA resource{};
	resource.pData = pData;
	resource.RowPitch = (width * cc);
	resource.SlicePitch = (width * height * cc);

	UpdateSubresources(cmdList, srv->texture->texture, srv->texture->textureUpload, 0, 0, 1, &resource);

	D3D12_RESOURCE_BARRIER resourceBarrier{};
	resourceBarrier.Transition.pResource = srv->texture->texture;
	resourceBarrier.Transition.StateBefore = D3D12_RESOURCE_STATE_COPY_DEST;
	resourceBarrier.Transition.StateAfter = D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE;

	cmdList->ResourceBarrier(1, &resourceBarrier);

	srv->cbvSrvUavHeapIndex = cbvSrvUavIndex;

	return srv;
}

Buffer* graphics::CreateBuffer(ID3D12Device* device, ID3D12GraphicsCommandList* cmdList, BufferDesc bufferDesc, bool uploadOnly) {

	Buffer* buffer = new Buffer();
	CD3DX12_HEAP_PROPERTIES heapPropertiesUpload  = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);
	CD3DX12_HEAP_PROPERTIES heapPropertiesDefault = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);
	CD3DX12_RESOURCE_DESC resourceDesc = CD3DX12_RESOURCE_DESC::Buffer(bufferDesc.totalBytes);

	device->CreateCommittedResource(&heapPropertiesUpload,
		D3D12_HEAP_FLAG_NONE,
		&resourceDesc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&buffer->vertexBufferUpload));

	if (!uploadOnly) {

		device->CreateCommittedResource(&heapPropertiesDefault,
			D3D12_HEAP_FLAG_NONE,
			&resourceDesc,
			D3D12_RESOURCE_STATE_COPY_DEST,
			nullptr,
			IID_PPV_ARGS(&buffer->vertexBuffer));

		if (bufferDesc.pData) {
			D3D12_SUBRESOURCE_DATA resource{};
			resource.pData = bufferDesc.pData;
			resource.RowPitch = bufferDesc.totalBytes;
			resource.SlicePitch = bufferDesc.totalBytes;

			UpdateSubresources(cmdList, buffer->vertexBuffer, buffer->vertexBufferUpload, 0, 0, 1, &resource);

			D3D12_RESOURCE_BARRIER resourceBarrier{};
			resourceBarrier.Transition.pResource = buffer->vertexBuffer;
			resourceBarrier.Transition.StateBefore = D3D12_RESOURCE_STATE_COPY_DEST;
			resourceBarrier.Transition.StateAfter = D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER;

			cmdList->ResourceBarrier(1, &resourceBarrier);
		}
	}

	return buffer;
}

