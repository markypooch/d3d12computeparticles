#pragma once

namespace math {
	struct Vector4D {
		float x, y, z, w;
	};
}