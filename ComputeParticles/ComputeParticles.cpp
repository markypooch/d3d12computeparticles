#include <Windows.h>
#include "D3D12GraphicsContext.h"
#define WIN32_LEAN_AND_MEAN

using namespace graphics;

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE pInstance, LPSTR lpCmdLine, int nCmdShow) {

	HWND hWnd;

	WNDCLASSEX wnd{};
	wnd.cbSize = sizeof(WNDCLASSEX);
	wnd.lpfnWndProc = WndProc;
	wnd.hInstance = hInstance;
	wnd.style = CS_HREDRAW | CS_VREDRAW;
	wnd.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wnd.lpszClassName = L"ComputeParticleWindow";

	if (!RegisterClassEx(&wnd)) {
		return -1;
	}

	hWnd = CreateWindowEx(0, L"ComputeParticleWindow", L"ComputeParticles", WS_OVERLAPPEDWINDOW, 0, 0, 3840, 2160, NULL, NULL, hInstance, NULL);

	ShowWindow(hWnd, nCmdShow);

	ContextDesc conDesc{};
	conDesc.height = 2160;
	conDesc.width = 3840;
	conDesc.hWnd = hWnd;
	conDesc.rr = 60;

	D3D12GraphicsContext d3d12GraphicsContext = D3D12GraphicsContext(conDesc);

	MSG msg{};
	while (msg.message != WM_QUIT) {
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else {
			d3d12GraphicsContext.DrawParticles();
		}
	}

	return 0;
}