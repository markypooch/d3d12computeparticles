#pragma once
#include "D3D12UtilityFunctions.h"
#include <d3d12.h>
#include "Vector2D.h"
#include "Vector4D.h"
#include <random>

struct Particles {
	math::Vector4D position[2000000];
	math::Vector4D direction[2000000];
	float speed[2000000];
	float currTime[2000000];
	float timeToLive[2000000];
};

class ParticleSystem
{
public:
	ParticleSystem(ID3D12Device* device, ID3D12GraphicsCommandList* cmdList, ID3D12DescriptorHeap* heap, size_t* currentDescriptorsAllocated, size_t cbvSrvUavDescriptorOffsetSize);
	~ParticleSystem();
public:
	void Update(float dt);
public:
	graphics::StructuredConstantBuffer* particlesSrv;
private:
	Particles* particles;
	void* pGpuMappedAddress;

	std::random_device               rd;
	std::default_random_engine       eng;
	std::uniform_real_distribution<> distri;
};

