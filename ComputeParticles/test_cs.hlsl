//Texture2D input0;
//Texture2D input1;

struct Particle {
	float4 position;
};
StructuredBuffer<Particle> input : register(t0);

cbuffer vp {
	matrix vp;
};

RWTexture2D<float4> output;

[numthreads(256, 1, 1)]
void main(int3 dispatchThreadID : SV_DispatchThreadID, int3 groupID : SV_GroupID) {
	
	int index = (groupID.x * 64) + dispatchThreadID.x;
	if (index < 2000000) {

		float4 projectedPosition = mul(vp, float4(input[index].position.xyz, 1.0f));
		
		// Frustum cull before perspective divide by checking of any of xyz are outside [-w, w]
		if (abs(projectedPosition.x) > abs(projectedPosition.w*2) ||
			abs(projectedPosition.y) > abs(projectedPosition.w*2) ||
			abs(projectedPosition.z) > abs(projectedPosition.w*2)) {
			return;
		}
	
		float3 position = projectedPosition.xyz / projectedPosition.w;

		position.x = (position.x / 2) * 0.5f;
		position.y = (position.y / -2) * 0.5f;

		int screenSpaceX = position.x * 3840.0f;
		int screenSpaceY = position.y * 2160.0f;

		float2 screenSpace = float2(screenSpaceX + 3840.0f / 2.0f, screenSpaceY + 2160.0f / 2.0f);

		if (position.z > 1.0f || position.z < 0.0f) {
			return;
		}

		float4 color = lerp(float4(1.0f, 0.15f, 0.0f, 1.0f), float4(0.0f, 0.2f, 1.0f, 1.0f), input[index].position.w);

		output[screenSpace] += color * (1-input[index].position.w);
	}
}