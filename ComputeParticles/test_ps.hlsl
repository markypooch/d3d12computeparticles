
Texture2D diffuse : register(t0);
SamplerState linearSampler : register(s0);

struct VertexOut {
	float4 pos : SV_POSITION;
	float2 tex : TEXCOORD;
};

float4 main(VertexOut vOut) : SV_TARGET{
	return diffuse.Sample(linearSampler, vOut.tex);
}