#pragma once
#include <d3d12.h>
#include "d3dx12.h"
namespace graphics {

	struct Texture {
		ID3D12Resource* texture;
		ID3D12Resource* textureUpload;
	};

	struct SrvUav {
		Texture* texture;
		size_t cbvSrvUavHeapIndex;
		size_t uavHeapIndex;
	};

	struct Buffer {
		ID3D12Resource* vertexBuffer;
		ID3D12Resource* vertexBufferUpload;
	};

	struct StructuredConstantBuffer {
		Buffer* buffer;
		size_t cbvSrvUavHeapIndex;
	};

	struct ScreenQuad {
		D3D12_VERTEX_BUFFER_VIEW vbView;
		Buffer* buffer;
	};

	struct BufferDesc {
		void* pData;
		size_t stride;
		size_t totalBytes;
	};

	SrvUav* CreateUav(ID3D12Device* device, ID3D12GraphicsCommandList* cmdList, ID3D12DescriptorHeap* heap, size_t *currentDescriptorsAllocated, unsigned int cbvSrvUavDescriptorOffsetSize, unsigned width, unsigned height, DXGI_FORMAT format);
	SrvUav* CreateSRVFromImage(ID3D12Device* device, ID3D12GraphicsCommandList* cmdList, D3D12_CPU_DESCRIPTOR_HANDLE cbvSrvUavHandle, unsigned int cbvSrvUavIndex, const char* fileName);
	Buffer* CreateBuffer(ID3D12Device* device, ID3D12GraphicsCommandList* cmdList, BufferDesc bufferDesc, bool);
};