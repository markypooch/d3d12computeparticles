#include "ParticlesSystem.h"
#include <random>
#include <ctime>

float RandomNormalizedFloat()
{
	return static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
}

void UniformRandomCircle(float* arr)
{

	float t = 2 * 3.14 * RandomNormalizedFloat();
	float u = RandomNormalizedFloat() + RandomNormalizedFloat();
	float r = (u > 1.0f) ? 2.0f - u : u;

	arr[0] = r * cos(t);
	arr[1] = r * sin(t);
}

ParticleSystem::ParticleSystem(ID3D12Device* device, ID3D12GraphicsCommandList* cmdList, ID3D12DescriptorHeap* heap, size_t* currentDescriptorsAllocated, size_t cbvSrvUavDescriptorOffsetSize) {

	CD3DX12_HEAP_PROPERTIES heapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);
	CD3DX12_RESOURCE_DESC resource = CD3DX12_RESOURCE_DESC::Buffer((sizeof(float)*4)* 2000000);

	particlesSrv = new graphics::StructuredConstantBuffer();
	particlesSrv->buffer = new graphics::Buffer();

	device->CreateCommittedResource(&heapProperties,
		D3D12_HEAP_FLAG_NONE,
		&resource,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&particlesSrv->buffer->vertexBufferUpload)
	);

	D3D12_BUFFER_SRV srv{};
	srv.FirstElement = 0;
	srv.NumElements = 2000000;
	srv.StructureByteStride = (sizeof(float)*4);
	srv.Flags = D3D12_BUFFER_SRV_FLAG_NONE;

	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc{};
	srvDesc.Buffer = srv;
	srvDesc.Format = DXGI_FORMAT_UNKNOWN;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_BUFFER;
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;

	D3D12_CPU_DESCRIPTOR_HANDLE handle = heap->GetCPUDescriptorHandleForHeapStart();
	handle.ptr = handle.ptr + (*currentDescriptorsAllocated * cbvSrvUavDescriptorOffsetSize);

	particlesSrv->cbvSrvUavHeapIndex = *currentDescriptorsAllocated;
	(*currentDescriptorsAllocated)++;

	device->CreateShaderResourceView(particlesSrv->buffer->vertexBufferUpload, &srvDesc, handle);

	CD3DX12_RANGE range(0, 0);
	particlesSrv->buffer->vertexBufferUpload->Map(0, &range, &pGpuMappedAddress);

	eng  =      std::default_random_engine(rd());
	distri = std::uniform_real_distribution<>(-1000, 1000);

	float speed = (distri(eng) / 1000.0f) * 20.0f + 100.0f;
	particles = new Particles();

	float y = 0;
	for (int i = 0; i < 2000000; i++) {

		if (i % 1000 == 0) {
			y++;
		}
		float initialPosX = 0.0f;
		float initialPosY = 0.0f;
		float initialPosZ = 0.0f;

		
		initialPosX = 0.0f;//distr(eng) / 1000.0f;
		initialPosY = 0.0f;// distr(eng) / 1000.0f;
		initialPosZ = 20.0f;

		float pos[2];
		UniformRandomCircle(pos);
		
		particles->position[i].x = pos[0];
		particles->position[i].y = pos[1];
		particles->position[i].z = 20.0f * (distri(eng)/1000.0f);
		particles->timeToLive[i] = (abs(distri(eng))/50.0f) + 5.0f;
		particles->position[i].w = 0.0f;

		particles->direction[i].x = 1.0f;
		particles->direction[i].y = 0.8f;
		particles->direction[i].z = (distri(eng) / 1000.0f);

		particles->speed[i] = (distri(eng) / 1000.0f) * 20.0f + 100.0f;
	}
}

void ParticleSystem::Update(float dt) {
	for (int i = 0; i < 2000000; i++) {
		particles->speed[i] -= 0.1 * (particles->currTime[i] / particles->timeToLive[i]);
		if (particles->speed[i] < 1.0f)
			particles->speed[i] = 1.0f;

		particles->position[i].x += (particles->direction[i].x / particles->speed[i]);
		particles->position[i].y += (particles->direction[i].y / particles->speed[i]);
		particles->position[i].z += (particles->direction[i].z / particles->speed[i]);
		particles->currTime[i] += dt;

		particles->position[i].w =  particles->currTime[i] / particles->timeToLive[i];

		if (particles->currTime[i] > particles->timeToLive[i]) {
			float pos[2];
			UniformRandomCircle(pos);

			particles->position[i].x = pos[0];
			particles->position[i].y = pos[1];
			particles->position[i].z = 20.0f * (distri(eng) / 1000.0f);
			particles->timeToLive[i] = (abs(distri(eng)) / 50.0f) + 5.0f;
			particles->position[i].w = 0.0f;

			particles->direction[i].x = 1.0f;
			particles->direction[i].y = 0.8f;
			particles->direction[i].z = (distri(eng) / 1000.0f);

			particles->currTime[i] = 0;

			particles->speed[i] = (distri(eng) / 1000.0f) * 20.0f + 100.0f;
		}
	}

	memcpy(pGpuMappedAddress, particles->position, (sizeof(float) * 4) * 2000000);
}