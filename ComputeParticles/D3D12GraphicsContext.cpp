#include "D3D12GraphicsContext.h"
using namespace graphics;

//graphics::Texture* graphics::CreateSRVFromImage(ID3D12Device* device, ID3D12GraphicsCommandList* cmdList, ID3D12DescriptorHeap* cbvSrvUavHeap, size_t* currentDescriptorsAllocated, size_t cbvSrvUavDescriptorOffsetSize, const char* fileName);
//graphics::Buffer*  graphics::CreateBuffer(ID3D12Device* device, ID3D12GraphicsCommandList* cmdList, BufferDesc bufferDesc, bool);

D3D12GraphicsContext::D3D12GraphicsContext(ContextDesc contextDesc) 
{
	CreateDXGIFactory1(IID_PPV_ARGS(&factory));
	D3D12CreateDevice(nullptr, D3D_FEATURE_LEVEL_12_0, IID_PPV_ARGS(&device));

	D3D12_COMMAND_QUEUE_DESC cmdQueueDesc{};
	cmdQueueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
	cmdQueueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;

	

	device->CreateCommandQueue(&cmdQueueDesc, IID_PPV_ARGS(&cmdQueue));

	DXGI_SWAP_CHAIN_DESC swapChainDesc{};
	swapChainDesc.BufferCount = 3;
	swapChainDesc.BufferDesc.Width = contextDesc.width;
	swapChainDesc.BufferDesc.Height = contextDesc.height;
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.BufferDesc.RefreshRate.Numerator = contextDesc.rr;
	swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.OutputWindow = contextDesc.hWnd;
	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.SampleDesc.Quality = 0;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	swapChainDesc.Windowed = true;

	IDXGISwapChain* swapChain0;
	factory->CreateSwapChain(cmdQueue, &swapChainDesc, &swapChain0);

	swapChain = static_cast<IDXGISwapChain3*>(swapChain0);

	D3D12_DESCRIPTOR_HEAP_DESC rtvDescriptorHeapDesc{};
	rtvDescriptorHeapDesc.NumDescriptors = 3;
	rtvDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
	rtvDescriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;

	device->CreateDescriptorHeap(&rtvDescriptorHeapDesc, IID_PPV_ARGS(&rtvDescriptorHeap));
	rtvDescriptorOffsetSize = device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);

	D3D12_DESCRIPTOR_HEAP_DESC cbvSrvUavDescriptorHeapDesc{};
	cbvSrvUavDescriptorHeapDesc.NumDescriptors = 512;
	cbvSrvUavDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	//cbvSrvUavDescriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;

	D3D12_DESCRIPTOR_HEAP_DESC cbvSrvUavDescriptorHeapDescShaderVisible{};
	cbvSrvUavDescriptorHeapDescShaderVisible.NumDescriptors = 512;
	cbvSrvUavDescriptorHeapDescShaderVisible.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	cbvSrvUavDescriptorHeapDescShaderVisible.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;

	device->CreateDescriptorHeap(&cbvSrvUavDescriptorHeapDesc, IID_PPV_ARGS(&cbvSrvUavNonShaderVisibleHeap));
	device->CreateDescriptorHeap(&cbvSrvUavDescriptorHeapDescShaderVisible, IID_PPV_ARGS(&cbvSrvUavShaderVisibleHeap));
	cbvSrvUavDescriptorOffsetSize = device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

	for (size_t i = 0; i < 3; i++) {

		swapChain->GetBuffer(i, IID_PPV_ARGS(&rtvTextures[i]));
		D3D12_CPU_DESCRIPTOR_HANDLE rtvHeapCpuHandle = rtvDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
		rtvHeapCpuHandle.ptr = rtvHeapCpuHandle.ptr + (i * rtvDescriptorOffsetSize);

		device->CreateRenderTargetView(rtvTextures[i], nullptr, rtvHeapCpuHandle);

		fenceValue[i] = 0;
		device->CreateFence(fenceValue[i], D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&fence[i]));

		device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&cmdAlloc[i]));
	}

	currentDescriptorsAllocated = 0;
	fenceEvent = CreateEvent(nullptr, false, false, L"");

	device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, cmdAlloc[0], nullptr, IID_PPV_ARGS(&cmdList));

	CreateScreenQuad();

	//D3D12_CPU_DESCRIPTOR_HANDLE cbvSrvUavHandle = cbvSrvUavNonShaderVisibleHeap->GetCPUDescriptorHandleForHeapStart();
	//cbvSrvUavHandle.ptr = cbvSrvUavHandle.ptr + (currentDescriptorsAllocated * cbvSrvUavDescriptorOffsetSize);

	CD3DX12_CPU_DESCRIPTOR_HANDLE cbvSrvUavHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		cbvSrvUavNonShaderVisibleHeap->GetCPUDescriptorHandleForHeapStart(),
		0,
		cbvSrvUavDescriptorOffsetSize
	);

	srv1 = CreateSRVFromImage(device, cmdList, cbvSrvUavHandle, currentDescriptorsAllocated, "test.png");
	currentDescriptorsAllocated++;

	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc{};
	srvDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	srvDesc.Texture2D.MipLevels = 1;
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;

	device->CreateShaderResourceView(srv1->texture->texture, &srvDesc, cbvSrvUavHandle);

	cbvSrvUavHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		cbvSrvUavNonShaderVisibleHeap->GetCPUDescriptorHandleForHeapStart(),
		1,
		cbvSrvUavDescriptorOffsetSize
	);

	srv2 = CreateSRVFromImage(device, cmdList, cbvSrvUavHandle, currentDescriptorsAllocated, "test2.png");
	currentDescriptorsAllocated++;

	srvDesc = {};
	srvDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	srvDesc.Texture2D.MipLevels = 1;
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;

	device->CreateShaderResourceView(srv2->texture->texture, &srvDesc, cbvSrvUavHandle);

	uav = CreateUav(device, cmdList, cbvSrvUavNonShaderVisibleHeap, &currentDescriptorsAllocated, cbvSrvUavDescriptorOffsetSize, 3840, 2160, DXGI_FORMAT_R8G8B8A8_UNORM);

	CreateConstantBuffer();

	CreateGraphicsRootSignature();
	CreateGraphicsPipeline();

	CreateComputeRootSignature();
	CreateComputePipeline();

	particleSystem = new ParticleSystem(device, cmdList, cbvSrvUavNonShaderVisibleHeap, &currentDescriptorsAllocated, cbvSrvUavDescriptorOffsetSize);

	cmdList->Close();
	ID3D12CommandList* c[] = { cmdList };
	cmdQueue->ExecuteCommandLists(1, c);

	fenceValue[0]++;

	cmdQueue->Signal(fence[0], fenceValue[0]);

	view = {};
	view.Height = contextDesc.height;
	view.Width = contextDesc.width;
	view.TopLeftY = 0.0f;
	view.TopLeftX = 0.0f;
	view.MinDepth = 0.0f;
	view.MaxDepth = 1.0f;

	scissorRect = {};
	scissorRect.top = 0;
	scissorRect.left = 0;
	scissorRect.right = contextDesc.width;
	scissorRect.bottom = contextDesc.height;
}

void D3D12GraphicsContext::DrawParticles() 
{
	particleSystem->Update(0.015f);

	currentFrameIndex = swapChain->GetCurrentBackBufferIndex();
	if (currentFrameIndex % 2 == 0) {
		tableOffset = 0;
	}

	if (fence[currentFrameIndex]->GetCompletedValue() < fenceValue[currentFrameIndex]) {
		fence[currentFrameIndex]->SetEventOnCompletion(fenceValue[currentFrameIndex], fenceEvent);
		WaitForSingleObject(fenceEvent, INFINITE);
	}

	cmdAlloc[currentFrameIndex]->Reset();
	cmdList->Reset(cmdAlloc[currentFrameIndex], nullptr);

	ID3D12DescriptorHeap* descriptorHeap[] = { cbvSrvUavShaderVisibleHeap };
	cmdList->SetDescriptorHeaps(1, descriptorHeap);

	BindComputeResources();

	CD3DX12_GPU_DESCRIPTOR_HANDLE gpuUavDescriptor = CD3DX12_GPU_DESCRIPTOR_HANDLE(
		cbvSrvUavShaderVisibleHeap->GetGPUDescriptorHandleForHeapStart(),
		tableOffset+2,
		cbvSrvUavDescriptorOffsetSize
	);

	CD3DX12_CPU_DESCRIPTOR_HANDLE cpuUavDescriptor = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		cbvSrvUavNonShaderVisibleHeap->GetCPUDescriptorHandleForHeapStart(),
		uav->uavHeapIndex,
		cbvSrvUavDescriptorOffsetSize
	);

	const float uavClearColor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };

	cmdList->ClearUnorderedAccessViewFloat(
		gpuUavDescriptor,
		cpuUavDescriptor,
		uav->texture->texture,
		uavClearColor,
		0,
		nullptr
	);

	D3D12_RESOURCE_BARRIER resourceBarrier{};
	resourceBarrier.Transition.pResource = uav->texture->texture;
	resourceBarrier.Transition.StateBefore = D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE;
	resourceBarrier.Transition.StateAfter = D3D12_RESOURCE_STATE_UNORDERED_ACCESS;

	cmdList->ResourceBarrier(1, &resourceBarrier);

	cmdList->SetComputeRootSignature(computeRootSig);
	cmdList->SetPipelineState(computePso);
	
	CD3DX12_GPU_DESCRIPTOR_HANDLE gpuHandle = CD3DX12_GPU_DESCRIPTOR_HANDLE(
		cbvSrvUavShaderVisibleHeap->GetGPUDescriptorHandleForHeapStart(),
		tableOffset,
		cbvSrvUavDescriptorOffsetSize
	);
	tableOffset += 16;

	cmdList->SetComputeRootDescriptorTable(0, gpuHandle);
	cmdList->Dispatch(10000, 1, 1);

	cmdList->RSSetViewports(1, &view);
	cmdList->RSSetScissorRects(1, &scissorRect);

	resourceBarrier = {};
	resourceBarrier.Transition.pResource = rtvTextures[currentFrameIndex];
	resourceBarrier.Transition.StateBefore = D3D12_RESOURCE_STATE_PRESENT;
	resourceBarrier.Transition.StateAfter = D3D12_RESOURCE_STATE_RENDER_TARGET;

	cmdList->ResourceBarrier(1, &resourceBarrier);

	resourceBarrier = {};
	resourceBarrier.Transition.pResource = uav->texture->texture;
	resourceBarrier.Transition.StateBefore = D3D12_RESOURCE_STATE_UNORDERED_ACCESS;
	resourceBarrier.Transition.StateAfter = D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE;

	cmdList->ResourceBarrier(1, &resourceBarrier);

	D3D12_CPU_DESCRIPTOR_HANDLE rtvDescriptorHandle = rtvDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
	rtvDescriptorHandle.ptr = rtvDescriptorHandle.ptr + (currentFrameIndex * rtvDescriptorOffsetSize);

	float clearColor[4] = { 0.2f, 0.4f, 0.6f, 1.0f };

	cmdList->OMSetRenderTargets(1, &rtvDescriptorHandle, false, nullptr);
	cmdList->ClearRenderTargetView(rtvDescriptorHandle, clearColor, 0, nullptr);

	cmdList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	cmdList->IASetVertexBuffers(0, 1, &screenQuad->vbView);

	cmdList->SetGraphicsRootSignature(rootSig);
	cmdList->SetPipelineState(pso);

	BindGraphicsResources();

	gpuHandle = CD3DX12_GPU_DESCRIPTOR_HANDLE(
		cbvSrvUavShaderVisibleHeap->GetGPUDescriptorHandleForHeapStart(),
		tableOffset,
		cbvSrvUavDescriptorOffsetSize
	);
	tableOffset += 16;

	cmdList->SetGraphicsRootDescriptorTable(0, gpuHandle);

	cmdList->DrawInstanced(6, 1, 0, 0);

	resourceBarrier = {};
	resourceBarrier.Transition.pResource = rtvTextures[currentFrameIndex];
	resourceBarrier.Transition.StateBefore = D3D12_RESOURCE_STATE_RENDER_TARGET;
	resourceBarrier.Transition.StateAfter = D3D12_RESOURCE_STATE_PRESENT;

	cmdList->ResourceBarrier(1, &resourceBarrier);

	cmdList->Close();
	ID3D12CommandList* c2[] = { cmdList };
	cmdQueue->ExecuteCommandLists(1, c2);

	fenceValue[currentFrameIndex]++;
	cmdQueue->Signal(fence[currentFrameIndex], fenceValue[currentFrameIndex]);

	swapChain->Present(0, 0);
}


void D3D12GraphicsContext::CreateConstantBuffer() {

	viewMat.SetIdentity();
	projection = math::ProjectionLH(3840.0f/2160.0f, 60*3.14/180, 0.1f, 100.0f);
	vp = math::MatrixMultiply(viewMat, projection);

	BufferDesc bufferDesc{};
	bufferDesc.pData = nullptr;
	bufferDesc.stride = 0;
	bufferDesc.totalBytes = 1024*64;

	viewProjCB.buffer = CreateBuffer(device, cmdList,bufferDesc, true);
	D3D12_CONSTANT_BUFFER_VIEW_DESC cbView{};
	cbView.BufferLocation = viewProjCB.buffer->vertexBufferUpload->GetGPUVirtualAddress();
	cbView.SizeInBytes = 256;

	CD3DX12_RANGE range(0, 0);
	viewProjCB.buffer->vertexBufferUpload->Map(0, &range, &pGpuMappedAddress);

	memcpy(pGpuMappedAddress, &vp, sizeof(ViewProj));

	D3D12_CPU_DESCRIPTOR_HANDLE cbvCpuHandle = cbvSrvUavNonShaderVisibleHeap->GetCPUDescriptorHandleForHeapStart();
	cbvCpuHandle.ptr = cbvCpuHandle.ptr + (currentDescriptorsAllocated * cbvSrvUavDescriptorOffsetSize);

	viewProjCB.cbvSrvUavHeapIndex = currentDescriptorsAllocated;

	currentDescriptorsAllocated++;
	device->CreateConstantBufferView(&cbView, cbvCpuHandle);
}


void D3D12GraphicsContext::CreateScreenQuad() {
	Vertex vertices[] = {
		{-1.0f, -1.0f, 0.5f, 1.0f, 0.0f, 1.0f},
		{-1.0f,  1.0f, 0.5f, 1.0f, 0.0f, 0.0f},
		{ 1.0f,  1.0f, 0.5f, 1.0f, 1.0f, 0.0f},
		{ 1.0f,  1.0f, 0.5f, 1.0f, 1.0f, 0.0f},
		{ 1.0f, -1.0f, 0.5f, 1.0f, 1.0f, 1.0f},
		{-1.0f, -1.0f, 0.5f, 1.0f, 0.0f, 1.0f}
	};

	BufferDesc bufferDesc{};
	bufferDesc.pData = vertices;
	bufferDesc.stride = sizeof(Vertex);
	bufferDesc.totalBytes = sizeof(Vertex) * 6;

	screenQuad = new ScreenQuad();
	screenQuad->buffer = CreateBuffer(device, cmdList, bufferDesc, false);
	D3D12_VERTEX_BUFFER_VIEW vbView{};
	vbView.BufferLocation = screenQuad->buffer->vertexBuffer->GetGPUVirtualAddress();
	vbView.StrideInBytes = sizeof(Vertex);
	vbView.SizeInBytes = sizeof(Vertex) * 6;

	screenQuad->vbView = vbView;
}

void D3D12GraphicsContext::CreateGraphicsRootSignature() {

	D3D12_DESCRIPTOR_RANGE range[1];
	range[0].BaseShaderRegister = 0;
	range[0].NumDescriptors = 1;
	range[0].OffsetInDescriptorsFromTableStart = 0;
	range[0].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	range[0].RegisterSpace = 0;

	D3D12_ROOT_DESCRIPTOR_TABLE table{};
	table.NumDescriptorRanges = 1;
	table.pDescriptorRanges = range;

	D3D12_ROOT_PARAMETER param{};
	param.DescriptorTable = table;
	param.ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;
	param.ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;

	D3D12_STATIC_SAMPLER_DESC samplerDesc{};
	samplerDesc.AddressU = samplerDesc.AddressV = samplerDesc.AddressW = D3D12_TEXTURE_ADDRESS_MODE_MIRROR;
	samplerDesc.ComparisonFunc = D3D12_COMPARISON_FUNC_LESS;
	samplerDesc.Filter = D3D12_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR;
	samplerDesc.MaxAnisotropy = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D12_FLOAT32_MAX;
	samplerDesc.RegisterSpace = 0;
	samplerDesc.ShaderRegister = 0;
	samplerDesc.ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	ID3DBlob* rootSigByteCode;
	CD3DX12_ROOT_SIGNATURE_DESC rootSigDesc = CD3DX12_ROOT_SIGNATURE_DESC(1, &param, 1, &samplerDesc, D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT);

	D3D12SerializeRootSignature(&rootSigDesc, D3D_ROOT_SIGNATURE_VERSION_1, &rootSigByteCode, nullptr);

	device->CreateRootSignature(0, rootSigByteCode->GetBufferPointer(), rootSigByteCode->GetBufferSize(), IID_PPV_ARGS(&rootSig));
}

void D3D12GraphicsContext::CreateGraphicsPipeline() {

	D3D12_INPUT_ELEMENT_DESC inputE[] = {
		{"POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
		{"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 16, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0}
	};

	D3D12_INPUT_LAYOUT_DESC inputL{};
	inputL.NumElements = 2;
	inputL.pInputElementDescs = inputE;

	D3D12_SHADER_BYTECODE psByteCode, vsByteCode;
	ID3DBlob* psByteCodeInterim, * vsByteCodeInterim;

	D3DCompileFromFile(L"test_vs.hlsl", nullptr, nullptr, "main", "vs_5_0", 0, 0, &vsByteCodeInterim, nullptr);
	D3DCompileFromFile(L"test_ps.hlsl", nullptr, nullptr, "main", "ps_5_0", 0, 0, &psByteCodeInterim, nullptr);

	vsByteCode.pShaderBytecode = vsByteCodeInterim->GetBufferPointer();
	vsByteCode.BytecodeLength = vsByteCodeInterim->GetBufferSize();

	psByteCode.pShaderBytecode = psByteCodeInterim->GetBufferPointer();
	psByteCode.BytecodeLength = psByteCodeInterim->GetBufferSize();

	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc{};
	psoDesc.pRootSignature = rootSig;
	psoDesc.VS = vsByteCode;
	psoDesc.PS = psByteCode;
	psoDesc.NumRenderTargets = 1;
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.InputLayout = inputL;
	psoDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
	psoDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.SampleDesc.Count = 1;
	psoDesc.SampleDesc.Quality = 0;
	psoDesc.SampleMask = 0xffffffff;

	device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&pso));
}

void D3D12GraphicsContext::CreateComputeRootSignature() {

	D3D12_DESCRIPTOR_RANGE range[3];
	range[0].BaseShaderRegister = 0;
	range[0].NumDescriptors = 1;
	range[0].OffsetInDescriptorsFromTableStart = 0;
	range[0].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	range[0].RegisterSpace = 0;

	range[1].BaseShaderRegister = 0;
	range[1].NumDescriptors = 1;
	range[1].OffsetInDescriptorsFromTableStart = 1;
	range[1].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_CBV;
	range[1].RegisterSpace = 0;

	range[2].BaseShaderRegister = 0;
	range[2].NumDescriptors = 1;
	range[2].OffsetInDescriptorsFromTableStart = 2;
	range[2].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_UAV;
	range[2].RegisterSpace = 0;

	D3D12_ROOT_DESCRIPTOR_TABLE table{};
	table.NumDescriptorRanges = 3;
	table.pDescriptorRanges = range;

	D3D12_ROOT_PARAMETER param{};
	param.DescriptorTable = table;
	param.ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;
	param.ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;

	D3D12_STATIC_SAMPLER_DESC samplerDesc{};
	samplerDesc.AddressU = samplerDesc.AddressV = samplerDesc.AddressW = D3D12_TEXTURE_ADDRESS_MODE_MIRROR;
	samplerDesc.ComparisonFunc = D3D12_COMPARISON_FUNC_LESS;
	samplerDesc.Filter = D3D12_FILTER_COMPARISON_MIN_MAG_MIP_POINT;
	samplerDesc.MaxAnisotropy = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D12_FLOAT32_MAX;
	samplerDesc.RegisterSpace = 0;
	samplerDesc.ShaderRegister = 0;
	samplerDesc.ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	ID3DBlob* rootSigByteCode;
	CD3DX12_ROOT_SIGNATURE_DESC rootSigDesc = CD3DX12_ROOT_SIGNATURE_DESC(1, &param, 1, &samplerDesc, D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT);

	D3D12SerializeRootSignature(&rootSigDesc, D3D_ROOT_SIGNATURE_VERSION_1, &rootSigByteCode, nullptr);

	device->CreateRootSignature(0, rootSigByteCode->GetBufferPointer(), rootSigByteCode->GetBufferSize(), IID_PPV_ARGS(&computeRootSig));
}

void D3D12GraphicsContext::CreateComputePipeline() {

	D3D12_SHADER_BYTECODE csByteCode;
	ID3DBlob* csByteCodeInterim, *error;

	D3DCompileFromFile(L"test_cs.hlsl", nullptr, nullptr, "main", "cs_5_0", D3DCOMPILE_DEBUG, D3DCOMPILE_SKIP_OPTIMIZATION, &csByteCodeInterim, &error);

	if (error) {
		const char* msg = (const char*)error->GetBufferPointer();
		int x = 10;
	}
	
	csByteCode.pShaderBytecode = csByteCodeInterim->GetBufferPointer();
	csByteCode.BytecodeLength  = csByteCodeInterim->GetBufferSize();

	D3D12_COMPUTE_PIPELINE_STATE_DESC psoDesc{};
	psoDesc.pRootSignature = computeRootSig;
	psoDesc.CS = csByteCode;
	psoDesc.Flags = D3D12_PIPELINE_STATE_FLAG_NONE;

	device->CreateComputePipelineState(&psoDesc, IID_PPV_ARGS(&computePso));
}

//These functions copy the descriptors into a heap that the shader can access in the order that 
//we defined our table layout in the rootsig creation
////////////////////////////////////////////////////////////////////////////////////////////////////////
void D3D12GraphicsContext::BindComputeResources() {
	CD3DX12_CPU_DESCRIPTOR_HANDLE dstHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		cbvSrvUavShaderVisibleHeap->GetCPUDescriptorHandleForHeapStart(),
		tableOffset,
		cbvSrvUavDescriptorOffsetSize
	);

	CD3DX12_CPU_DESCRIPTOR_HANDLE srcHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		cbvSrvUavNonShaderVisibleHeap->GetCPUDescriptorHandleForHeapStart(),
		particleSystem->particlesSrv->cbvSrvUavHeapIndex,
		cbvSrvUavDescriptorOffsetSize
	);

	device->CopyDescriptorsSimple(1, dstHandle, srcHandle, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

	dstHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		cbvSrvUavShaderVisibleHeap->GetCPUDescriptorHandleForHeapStart(),
		tableOffset+1,
		cbvSrvUavDescriptorOffsetSize
	);

	srcHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		cbvSrvUavNonShaderVisibleHeap->GetCPUDescriptorHandleForHeapStart(),
		viewProjCB.cbvSrvUavHeapIndex,
		cbvSrvUavDescriptorOffsetSize
	);

	device->CopyDescriptorsSimple(1, dstHandle, srcHandle, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

	dstHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		cbvSrvUavShaderVisibleHeap->GetCPUDescriptorHandleForHeapStart(),
		tableOffset + 2,
		cbvSrvUavDescriptorOffsetSize
	);

	srcHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		cbvSrvUavNonShaderVisibleHeap->GetCPUDescriptorHandleForHeapStart(),
		uav->uavHeapIndex,
		cbvSrvUavDescriptorOffsetSize
	);

	device->CopyDescriptorsSimple(1, dstHandle, srcHandle, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
}

void D3D12GraphicsContext::BindGraphicsResources() {
	CD3DX12_CPU_DESCRIPTOR_HANDLE dstHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		cbvSrvUavShaderVisibleHeap->GetCPUDescriptorHandleForHeapStart(),
		tableOffset,
		cbvSrvUavDescriptorOffsetSize
	);

	CD3DX12_CPU_DESCRIPTOR_HANDLE srcHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		cbvSrvUavNonShaderVisibleHeap->GetCPUDescriptorHandleForHeapStart(),
		uav->cbvSrvUavHeapIndex,
		cbvSrvUavDescriptorOffsetSize
	);

	device->CopyDescriptorsSimple(1, dstHandle, srcHandle, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
}